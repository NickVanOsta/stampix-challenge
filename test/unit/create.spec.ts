/* eslint-disable @typescript-eslint/no-explicit-any */
import container from '../../src/di/inversify.config';
import TYPES from '../../src/di/types';
import { MockUserRepo } from './mocks/repo/user';

import { handler } from '../../src/create';

describe('create user tests', () => {
  beforeAll(() => {
    container.rebind(TYPES.UserRepo).toConstantValue(new MockUserRepo());
  });

  describe('valid user', () => {
    const event: { body: string } = {
      body:
        '{\n  "firstName": "Nick",\n  "lastName": "Van Osta",\n  "gender": "male",\n  "email": "nick.vanosta@ugent.be",\n  "phoneNumber": "+32494797817",\n  "dateOfBirth": "02/07/1998",\n  "language": "nl"\n}',
    };
    let res: any;

    beforeAll(async () => {
      res = await handler(event);
    });

    it('returns the created user', () => {
      // check if all the keys of the request body are equal to the corresponding keys of the response body
      const req = JSON.parse(event.body);
      Object.keys(req).forEach((k: string) => {
        expect(JSON.parse(res.body)[k]).toBe(req[k]);
      });
    });
    it('returns status code 201', () => {
      expect(res.statusCode).toBe(201);
    });
    it('returns location header', () => {
      expect(res.headers.Location).toBe(`/user/${JSON.parse(res.body).id}`);
    });
  });

  describe('invalid user', () => {
    const event = { body: '{}' };
    let res: any;

    beforeAll(async () => {
      res = await handler(event);
    });

    it('returns no response body', () => {
      expect(res.body).toBeFalsy();
    });
    it('returns status code 400', () => {
      expect(res.statusCode).toBe(400);
    });
  });
});
