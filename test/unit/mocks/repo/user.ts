/* eslint-disable @typescript-eslint/no-unused-vars */
import { User } from '../../../../src/model';
import type { UserRepo } from '../../../../src/repo';

export const users: { [key: number]: User } = {
  1: new User(
    1,
    'male',
    'Nick',
    'Van Osta',
    'nick.vanosta@ugent.be',
    '+32494797817',
    '02/07/1998',
    'nl',
    new Date(),
    new Date()
  ),
  2: new User(
    2,
    'male',
    'Alan',
    'Turing',
    'alan.turing@cambridge.uk',
    'numbersnumbers',
    '23/06/1927',
    'en',
    new Date(),
    new Date()
  ),
};

/**
 * Mocks the user repository with a simple in-memory map.
 *
 * @author Nick Van Osta
 */
export class MockUserRepo implements UserRepo {
  async getAllUsers(): Promise<User[]> {
    return Object.values(users);
  }
  async getUsersByFirstName(firstName: string): Promise<User[]> {
    return Object.values(users).filter(
      (x) => x.firstName.toUpperCase() === firstName.toUpperCase()
    );
  }
  async getUserByID(id: number): Promise<User> {
    return users[id];
  }
  async createUser(user: User): Promise<User> {
    return user;
  }
}

/**
 * Mocks the user repository as if it were empty.
 *
 * @author Nick Van Osta
 */
export class MockEmptyUserRepo implements UserRepo {
  async getAllUsers(): Promise<User[]> {
    return [];
  }
  async getUsersByFirstName(_firstName: string): Promise<User[]> {
    return [];
  }
  async getUserByID(_id: number): Promise<User> {
    throw null;
  }
  async createUser(user: User): Promise<User> {
    return user;
  }
}
