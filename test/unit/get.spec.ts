/* eslint-disable @typescript-eslint/no-explicit-any */
import container from '../../src/di/inversify.config';
import TYPES from '../../src/di/types';
import { MockUserRepo } from './mocks/repo/user';

import { handler } from '../../src/get';

describe('get user tests', () => {
  beforeAll(() => {
    container.rebind(TYPES.UserRepo).toConstantValue(new MockUserRepo());
  });

  describe('valid id', () => {
    const event = { pathParameters: { id: 1 } };
    let res: any;

    beforeAll(async () => {
      res = await handler(event);
    });

    it('returns the requested user', () => {
      expect(JSON.parse(res.body).id).toBe(event.pathParameters.id);
    });
    it('returns status code 200', () => {
      expect(res.statusCode).toBe(200);
    });
  });

  describe('invalid id', () => {
    const event = { pathParameters: { id: -1 } };
    let res: any;

    beforeAll(async () => {
      res = await handler(event);
    });

    it('returns no response body', () => {
      expect(res.body).toBeFalsy();
    });
    it('returns status code 404', () => {
      expect(res.statusCode).toBe(404);
    });
  });
});
