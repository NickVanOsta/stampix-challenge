/* eslint-disable @typescript-eslint/no-explicit-any */
import container from '../../src/di/inversify.config';
import TYPES from '../../src/di/types';
import { MockEmptyUserRepo, MockUserRepo, users } from './mocks/repo/user';

import { handler } from '../../src/list';

describe('list users tests', () => {
  describe('filled with data', () => {
    beforeAll(() => {
      // rebind repo
      container.rebind(TYPES.UserRepo).toConstantValue(new MockUserRepo());
    });

    describe("without 'first' queryString parameter", () => {
      const event = { queryStringParameters: { first: '' } };
      let res: any;

      beforeAll(async () => {
        // run handler
        res = await handler(event);
      });

      it('returns a list of users', () => {
        expect(JSON.parse(res.body).length).toBe(Object.values(users).length);
      });
      it('returns status code 200', () => {
        expect(res.statusCode).toBe(200);
      });
    });

    describe("with valid 'first' queryString parameter", () => {
      const event = { queryStringParameters: { first: 'NicK' } };
      let res: any;

      beforeAll(async () => {
        // run handler
        res = await handler(event);
      });

      it('returns a list of users filtered on the first name', () => {
        expect(JSON.parse(res.body).length).toBe(1);

        JSON.parse(res.body).forEach((u: any) =>
          expect(u.firstName.toUpperCase()).toBe(
            event.queryStringParameters.first.toUpperCase()
          )
        );
      });
      it('returns status code 200', () => {
        expect(res.statusCode).toBe(200);
      });
    });

    describe("with invalid 'first' queryString parameter", () => {
      const event = { queryStringParameters: { first: 'Ovuvuevuevue' } };
      let res: any;

      beforeAll(async () => {
        // run handler
        res = await handler(event);
      });

      it('returns an empty list', () => {
        expect(JSON.parse(res.body).length).toBe(0);
      });
      it('returns status code 204', () => {
        expect(res.statusCode).toBe(204);
      });
    });
  });

  describe('no data', () => {
    beforeAll(() => {
      // rebind repo
      container.rebind(TYPES.UserRepo).toConstantValue(new MockEmptyUserRepo());
    });

    it('returns an empty list and status code 204 when no data is present', async () => {
      // run handler
      const event = { queryStringParameters: { first: '' } };
      const res = await handler(event);

      expect(res.statusCode).toBe(204);
      expect(JSON.parse(res.body).length).toEqual(0);
    });

    describe("without 'first' queryString parameter", () => {
      const event = { queryStringParameters: { first: '' } };
      let res: any;

      beforeAll(async () => {
        // run handler
        res = await handler(event);
      });

      it('returns an empty list', () => {
        expect(JSON.parse(res.body).length).toBe(0);
      });
      it('returns status code 204', () => {
        expect(res.statusCode).toBe(204);
      });
    });

    describe("with valid 'first' queryString parameter", () => {
      const event = { queryStringParameters: { first: 'NicK' } };
      let res: any;

      beforeAll(async () => {
        // run handler
        res = await handler(event);
      });

      it('returns an empty list', () => {
        expect(JSON.parse(res.body).length).toBe(0);
      });
      it('returns status code 204', () => {
        expect(res.statusCode).toBe(204);
      });
    });

    describe("with invalid 'first' queryString parameter", () => {
      const event = { queryStringParameters: { first: 'Ovuvuevuevue' } };
      let res: any;

      beforeAll(async () => {
        // run handler
        res = await handler(event);
      });

      it('returns an empty list', () => {
        expect(JSON.parse(res.body).length).toBe(0);
      });
      it('returns status code 204', () => {
        expect(res.statusCode).toBe(204);
      });
    });
  });
});
