module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',

  collectCoverage: true,
  collectCoverageFrom: ['**/src/**/*.{js, jsx,ts}', '!**/node_modules/**'],
  coverageDirectory: 'test-reports',
  coverageReporters: ['text'],
  testResultsProcessor: 'jest-junit',
  reporters: ['default', 'jest-junit'],
};
