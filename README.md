# Stampix Node.js & Serverless challenge

## Setting
Cloud-based AWS Lambda functions with a Node.js 12 runtime written in TypeScript.

## Goal
This repository contains a populated SQLite database consisting of 50 users. The goal is to create 3 functions, corresponding to 4 operations on the database: creation, retrieving, listing & searching.

The following REST API is set up already for you (see Practical):

1. A list of users: `GET http://localhost:3000/users`
2. Get a specific user: `GET http://localhost:3000/user/{id}`
3. Create a user: `POST http://localhost:3000/user`

Endpoint one should both be able to:

1. List all users
2. Find all users with a specific first name

Endpoint two and three are straight-forward.

## Practical
1. Clone this repo and install the already existing development dependencies.
3. Write your functions according to AWS Lambda standards using the Node.js 12 runtime inside `/src/*.ts`
4. Start the compiler using `yarn watch`, this will pro-actively compile your changes .
5. In another terminal, run a local server using `yarn start`, this will make the local endpoints available.

## Requirements
1. The 3 functions should do what they are supposed to do according to the goals of this challenge.
2. You should add automatic testing and make sure that `yarn test` does what could be expected of it in a CI/CD context.
3. Document your code as you would in any collaborative project

## Food for thought
Think about the following questions and formulate an answer below. Think about the difficulties or edge-cases you would encounter. How would you tackle these?

The number of users suddenly increases to over 10,000. What comes to your mind with relationship to the functionality you just wrote?

Have a look at the datastructure of the database (i.e. have a look at `scripts/populate.js`). Now assume that we are not using SQLite, but MySQL 8. What would you do differently?

Suppose we want to set up a search function for the users where we can search with an arbitrary input value. How would you do this?

### Your answers

#### Over 10.000 users
This would create long delays on GET requests, especially for all users.
I would solve this by adding pagination, limiting the amount of users fetched from the database and sent over the network.

#### MySQL 8 <> SQLite3
I would solve this by creating a new folder `src/repo/mysql8` where I would implement the MySQL8 version of the repository interfaces.
This would probably require a new package to be installed.
The only changes to make to migrate is to import the new MySQL8 implementations in `src/di/inversify.config.ts` and bind them to the appropriate repository interfaces.

#### Arbitrary search function
I would add a querystring parameter `q` to the request. This parameter would have a string value which I could pass down to the repositories.
There I would filter the SQL query on the filter:

```sql
SELECT
  id AS id,
  gender AS gender,
  first_name AS firstName,
  last_name AS lastName,
  email AS email,
  phone_number AS phoneNumber,
  date_of_birth AS dateOfBirth,
  language AS language,
  created_at AS createdAt,
  modified_at AS modifiedAt
FROM user
WHERE
  gender        LIKE '%'||?||'%' OR
  first_name    LIKE '%'||?||'%' OR
  last_name     LIKE '%'||?||'%' OR
  email         LIKE '%'||?||'%' OR
  phone_number  LIKE '%'||?||'%' OR
  date_of_birth LIKE '%'||?||'%' OR
  language      LIKE '%'||?||'%'
```

If I had enough time to implement it, I would try going for a fuzzy search where typos are accounted for.

## Delivery
- Anything that is not enforced in the base repository (tools, dependencies, architecture, frameworks, ...) is free of choice
- Push your solution to your Git platform of choice, as long as it's public.

**Note**: There's no need to actually use the AWS Cloud.
