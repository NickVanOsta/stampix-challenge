import { Container } from 'inversify';
import TYPES from './types';

// interface types
import type { UserRepo } from '../repo';

// interface implementations
import { SQLite3UserRepo } from '../repo/sqlite3';

// bindings
const container = new Container();
container.bind<UserRepo>(TYPES.UserRepo).to(SQLite3UserRepo);

export default container;
