import { isUserType, User } from './model';
import container from './di/inversify.config';
import TYPES from './di/types';

import type { UserRepo } from './repo';

/**
 * Handler for the `/users/{id}` endpoint.
 *
 * @author Nick Van Osta
 */
export const handler = async (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  event: any
): Promise<{
  statusCode: number;
  body: string | undefined;
  headers: { [key: string]: string | undefined };
}> => {
  // get repository
  const userRepo: UserRepo = container.get<UserRepo>(TYPES.UserRepo);

  // create user
  const req: User = JSON.parse(event.body);
  const user = isUserType(req) ? await userRepo.createUser(req) : undefined;

  return {
    statusCode: user ? 201 : 400,
    body: user ? JSON.stringify(user) : undefined,
    headers: {
      Location: user ? `/user/${user.id}` : undefined,
    },
  };
};
