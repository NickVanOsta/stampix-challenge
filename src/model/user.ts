/**
 * Represents a User.
 *
 * @author Nick Van Osta
 */
export class User {
  id?: number | null;
  gender: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  dateOfBirth: string;
  language: string;
  createdAt?: Date | null;
  modifiedAt?: Date | null;

  constructor(
    id: number,
    gender: string,
    firstName: string,
    lastName: string,
    email: string,
    phoneNumber: string,
    dateOfBirth: string,
    language: string,
    createdAt: Date,
    modifiedAt: Date
  ) {
    this.id = id;
    this.gender = gender;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.dateOfBirth = dateOfBirth;
    this.language = language;
    this.createdAt = createdAt;
    this.modifiedAt = modifiedAt;
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-explicit-any
export function isUserType(o: any): o is User {
  return (
    'gender' in o &&
    'firstName' in o &&
    'lastName' in o &&
    'email' in o &&
    'phoneNumber' in o &&
    'dateOfBirth' in o &&
    'language' in o
  );
}
