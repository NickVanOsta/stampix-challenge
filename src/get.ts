import container from './di/inversify.config';
import TYPES from './di/types';

import type { UserRepo } from './repo';

/**
 * Handler for the `/users/{id}` endpoint.
 *
 * @author Nick Van Osta
 */
export const handler = async (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  event: any
): Promise<{
  statusCode: number;
  body: string;
}> => {
  // get repository
  const userRepo: UserRepo = container.get<UserRepo>(TYPES.UserRepo);

  // retrieve correct user
  const user = await userRepo.getUserByID(event.pathParameters.id);

  return {
    statusCode: user ? 200 : 404,
    body: JSON.stringify(user),
  };
};
