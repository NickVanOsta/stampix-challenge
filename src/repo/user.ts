import type { User } from '../model';

/**
 * Represents a generic User repository.
 * @interface
 *
 * @author Nick Van Osta
 */
export interface UserRepo {
  /**
   * Retrieve all users.
   * @function
   */
  getAllUsers(): Promise<User[]>;

  /**
   * Retrieve all users with the specified first name.
   * @function
   *
   * @param firstName the first name to filter on.
   */
  getUsersByFirstName(firstName: string): Promise<User[]>;

  /**
   * Retrieve the user with the specified ID.
   * @function
   *
   * @param id the user's unique identifier
   */
  getUserByID(id: number): Promise<User>;

  /**
   * Create a new user.
   * @function
   *
   * @param user the user to be created
   */
  createUser(user: User): Promise<User>;
}
