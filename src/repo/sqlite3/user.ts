import 'reflect-metadata';
import { injectable } from 'inversify';

import sqlite3 from 'sqlite3';
import path from 'path';

import type { User } from '../../model';
import type { UserRepo } from '..';

// queries
const GET_USERS = `
SELECT
  id AS id,
  gender AS gender,
  first_name AS firstName,
  last_name AS lastName,
  email AS email,
  phone_number AS phoneNumber,
  date_of_birth AS dateOfBirth,
  language AS language,
  created_at AS createdAt,
  modified_at AS modifiedAt
FROM user
`;

const GET_ALL = `${GET_USERS};`;
const GET_BY_FIRST_NAME = `${GET_USERS} WHERE UPPER(first_name) = UPPER(?);`;
const GET_BY_ID = `${GET_USERS} WHERE id = ?;`;

const INSERT = `INSERT INTO user (gender, first_name, last_name, email, phone_number, date_of_birth, language) VALUES (?, ?, ?, ?, ?, ?, ?);`;

/**
 * SQLite 3 implementation of the UserRepo interface.
 * This repository uses a local database file that can be found in the root of the project.
 *
 * @implements UserRepo
 *
 * @author Nick Van Osta
 */
@injectable()
export class SQLite3UserRepo implements UserRepo {
  dbFile: string = path.join(__dirname, '../../../db.sqlite');
  db: sqlite3.Database;

  constructor() {
    this.db = new sqlite3.Database(this.dbFile);
  }

  /**
   * Retrieve all users from the database.
   * @function
   *
   * @author Nick Van Osta
   */
  getAllUsers(): Promise<User[]> {
    return new Promise((resolve, reject) => {
      this.db.all(GET_ALL, (err, rows) => {
        // log errors
        if (err) {
          console.log(err);
          reject(err);
        }

        // return rows
        resolve(rows);
      });
    });
  }

  /**
   * Retrieve all users with the specified first name from the database.
   * @function
   *
   * @param firstName the user's first name.
   */
  getUsersByFirstName(firstName: string): Promise<User[]> {
    return new Promise((resolve, reject) => {
      this.db.all(GET_BY_FIRST_NAME, firstName, (err, rows) => {
        // log errors
        if (err) {
          console.log(err);
          reject(err);
        }

        // return rows
        resolve(rows);
      });
    });
  }

  /**
   * Retrieve the user with the specified ID from the database.
   * @function
   *
   * @param id the user's unique identifier.
   */
  getUserByID(id: number): Promise<User> {
    return new Promise((resolve, reject) => {
      this.db.get(GET_BY_ID, id, (err, row) => {
        // log errors
        if (err) {
          console.log(err);
          reject(err);
        }

        // return rows
        resolve(row);
      });
    });
  }

  /**
   * Create a new user in the database.
   * @function
   *
   * @param user the user to be created
   */
  createUser(user: User): Promise<User> {
    return new Promise((resolve, reject) => {
      const stmt: sqlite3.Statement & {
        lastID?: number;
      } = this.db.prepare(
        INSERT,
        user.gender,
        user.firstName,
        user.lastName,
        user.email,
        user.phoneNumber,
        user.dateOfBirth,
        user.language
      );

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      stmt.run((err: any) => {
        // log errors
        if (err) {
          console.log(err);
          reject(err);
        }

        // retrieve the created user
        this.db.get(GET_BY_ID, stmt.lastID, (err, row) => {
          // log errors
          if (err) {
            console.log(err);
            reject(err);
          }

          // return created user
          resolve(row);
        });
      });
    });
  }
}
