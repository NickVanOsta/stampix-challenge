import container from './di/inversify.config';
import TYPES from './di/types';

import type { UserRepo } from './repo';

/**
 * Handler for the `/users` endpoint.
 *
 * @author Nick Van Osta
 */
export const handler = async (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  event: any
): Promise<{
  statusCode: number;
  body: string;
}> => {
  // get repository
  const userRepo: UserRepo = container.get<UserRepo>(TYPES.UserRepo);

  // retrieve users
  const users =
    event.queryStringParameters && event.queryStringParameters.first
      ? await userRepo.getUsersByFirstName(event.queryStringParameters.first)
      : await userRepo.getAllUsers();

  return {
    // return 204 (No Content) when the array is empty, otherwise return 200 (OK)
    statusCode: users.length === 0 ? 204 : 200,
    body: JSON.stringify(users),
  };
};
